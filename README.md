# btrbk

A tool for managing btrfs snapshots.

> This image is tested for use with **podman** on **Linux** hosts with
> **x86_64** arch


## What is btrbk?

Btrbk is a backup tool for btrfs subvolumes, taking advantage of btrfs specific
capabilities to create atomic snapshots and transfer them incrementally to your
backup locations.

See [the project website](https://github.com/digint/btrbk)


## How to use this image

This image is meant to be a one-to-one replacement of a natively installed
`btrbk`. It is primarily intended for people running distributions that don't
come with perl preinstalled. On a Fedora Silverblue, installing perl will pull
in ~250 additional packages as dependencies, which IMO isn't nice.

To run btrbk, you must have a configuration file. Since generating a
configuration isn't exactly trivial with all the options that `btrbk` offers,
please refer to the [official documentation on *Configuration*][1] **and read
the section about "Generating a valid configuration" below!**

Once you have a configuration (stored in e.g. `/etc/btrbk/default.conf`),
you can run the container like so:


```bash
$ sudo podman run --rm -i --privileged -v "/:/hostfs" -v "/etc/btrbk/default.conf:/btrbk.conf" registry.gitlab.com/c8160/btrbk:latest -c /btrbk.conf -v run
```


## Generating a valid configuration

When creating your configuration, please keep in mind that btrbk is run in a
container and **doesn't have the same filesystem paths as your host**. In the
example below we assume that you mount your entire rootfs to the path `/hostfs`
in the container. Here's an example to clarify the issue:

Assume you want to take snapshots of your `/home` to a snapshot folder
`/snapshots` (This is referred to as the *time-machine* setup in the btrbk
docs). The configuration with btrbk installed on your host looks like this:

```
volume /
  snapshot_dir snapshots
  subvolume home
```

**When using the container** we slightly modify this to look like this:

```
volume /hostfs/
  snapshot_dir snapshots
  subvolume home
```


## Getting help

First you may want to refer to the `help.md` contained in this repository or
the containers rootfs. If you're still stuck or found an issue with the
container in particular, feel free to open an issue on Gitlab:
https://gitlab.com/c8160/btrbk/-/issues


[1]: https://github.com/digint/btrbk#configuration
