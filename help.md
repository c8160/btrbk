btrbk image, for managing btrfs subvolumes and snapshots.

Packages the [btrbk Github project](https://github.com/digint/btrbk), in
particular the `btrbk` executable which is also the entrypoint of this
container.

Volumes:
hostfs: Mapped to /hostfs in the container, contains the btrfs filesystems to
  work on. btrbk cannot run without this. Must be mapped read/write.

Documentation:
For the underlying btrbk project, see https://github.com/digint/btrbk
For this container, see https://gitlab.com/c8160/btrbk

Requirements:
Works only on btrfs filesystems, needs full system privileges to access the
filesystem.

Configuration:
Please refer to the [configuration section of btrbk][1] and the additional
hints mentioned in the [container projects README][2]


[1]: https://github.com/digint/btrbk#configuration
[2]: https://gitlab.com/c8160/btrbk/-/blob/main/README.md
